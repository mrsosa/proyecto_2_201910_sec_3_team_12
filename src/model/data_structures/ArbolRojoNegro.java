package model.data_structures;

import java.util.NoSuchElementException;
/**
 * Clase que representa un BST balanceado rojo-negro 
 * @author mrSosa
 * @param <K> Tipo generico de la llave del arbol
 * @param <V> Tipo generico del arbol
 * Referencias Codigo:: https://algs4.cs.princeton.edu/home/ (Pagina oficial del libro Algorithms, 4th Edition)
 * Adaptado de: https://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 */
public class ArbolRojoNegro<K extends Comparable<K>, V> 
{

	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	private NodoArbol raiz;

	private class NodoArbol 
	{
		private K key;           // llave
		private V value;         // valor
		private NodoArbol left, right;  
		private boolean color;     // color del padre = negro o hermano = rojo
		private int N;          // numero de subnodos

		public NodoArbol(K key, V val, int n, boolean color) {
			this.key = key;
			this.value = val;
			this.color = color;
			this.N = n;
		}
	}
	/**
	 * Inicializa el arbol.
	 */
	public ArbolRojoNegro(){
	}

	/***************************************************************************
	 *  Metodos de ayuda a los nodos.
	 ***************************************************************************/
	/**
	 * El nodo x es rojo?
	 * @param x Nodo
	 * @return true si sies rojo, false en caso contrario.
	 */
	private boolean isRed(NodoArbol x){
		if(x == null) return false;
		return x.color == RED;
	}

	/**
	 * Tama�o del arbol.
	 * @return tama�o
	 */
	public int size(){
		return size(raiz);
	}

	private int size(NodoArbol x){
		if(x == null) return 0;
		return x.N;
	}

	/**
	 * Esta vacio el arbol?
	 * @return true si esta vacio - false en caso contrario.
	 */
	public boolean isEmpty(){
		return raiz == null;
	}

	/***************************************************************************
	 *  Metodos standard de busqueda.
	 ***************************************************************************/

	/**
	 * Dar valor segun la llave.
	 * @param key llave
	 * @return valor segun la llave.
	 */
	public V get(K key){
		if (key == null) throw new IllegalArgumentException("Lallave a buscar es null");
		return get(raiz, key);
	}

	private V get(NodoArbol x, K key){
		while(x != null){
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.value;
		}
		return null;
	}

	/**
	 * Contiene la llave?
	 * @param key Llave
	 * @return True si existe en el arbol la llave - false en caso contrario.
	 * @throws Exception El arbol esta vacio
	 */
	public boolean contains(K key) {
		return get(key) != null;
	}

	/***************************************************************************
	 *  Metodos standard de insercion.
	 ***************************************************************************/

	/**
	 * Agrega un nuevo elemento al arbol.
	 * @param key Llave
	 * @param value Valor
	 */
	public void put(K key, V value){
		if (key == null) throw new IllegalArgumentException("La llave a eliminar es null");
		if (value == null) {
			delete(key);
			return;
		}

		raiz = put(raiz, key, value);
		raiz.color = BLACK;
	}

	private NodoArbol put(NodoArbol h, K key, V value){
		if(h == null) return new NodoArbol(key, value, 1, RED);

		int cmp = key.compareTo(h.key);
		if	   (cmp < 0) h.left = put(h.left, key, value);
		else if(cmp > 0) h.right = put(h.right, key, value);
		else h.value = value;

		if(isRed(h.right) && !isRed(h.left))  	h = rotateLeft(h);
		if(isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if(isRed(h.left) && isRed(h.right))  flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}

	/***************************************************************************
	 *  Metodos para eliminar.
	 ***************************************************************************/
	/**
	 * Elimina el menor nodo.
	 */
	public void deleteMin(){
		if (isEmpty()) throw new NoSuchElementException("BST Est� vacio");

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = deleteMin(raiz);
		if(!isEmpty()) raiz.color = BLACK;
	}

	private NodoArbol deleteMin(NodoArbol h) 
	{ 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}

	/**
	 * Elimina el mayor.
	 */
	public void deleteMax()
	{
		if (isEmpty()) throw new NoSuchElementException("BST Est� vacio");

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = deleteMax(raiz);
		if(!isEmpty()) raiz.color = BLACK;
	}
    /**
     * Elimina el mayor a partir de un nodo pasado por parametro.
     * @param h
     * @return n
     */
	private NodoArbol deleteMax(NodoArbol h) { 
		if (isRed(h.left)) h = rotateRight(h);

		if (h.right == null) return null;

		if (!isRed(h.right) && !isRed(h.right.left)) h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}

	/**
	 * Elimina el elemento de acuerdo a la llave. 
	 * @param key Llave
	 */
	public void delete(K key){
		if (key == null) throw new IllegalArgumentException("La llave a eliminar es null");
		if (!contains(key)) return;

		if (!isRed(raiz.left) && !isRed(raiz.right)) raiz.color = RED;

		raiz = delete(raiz, key);
		if(!isEmpty()) raiz.color = BLACK;
	}
	
	
	private NodoArbol delete(NodoArbol h, K key){
		if (key.compareTo(h.key) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left)) h = moveRedLeft(h);
			h.left = delete(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotateRight(h);
			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.key) == 0) {
				NodoArbol x = min(h.right);
				h.key = x.key;
				h.value = x.value;
				h.right = deleteMin(h.right);
			}
			else h.right = delete(h.right, key);
		}
		return balance(h);
	}

	/***************************************************************************
	 *  Metodos de apoyo para el arbol.
	 ***************************************************************************/

	private NodoArbol rotateLeft(NodoArbol h){
		NodoArbol  x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)+ size(h.right);
		return x;		
	}

	private NodoArbol rotateRight(NodoArbol h){
		NodoArbol x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.N = h.N;
		h.N = 1 + size(h.left)+ size(h.right);
		return x;
	}

	private void flipColors(NodoArbol h){
		h.color = !h.color;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}

	private NodoArbol moveRedLeft(NodoArbol h) {
		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotateRight(h.right);
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol moveRedRight(NodoArbol h) {
		flipColors(h);
		if (isRed(h.left.left)) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol balance(NodoArbol h) {
		if (isRed(h.right))                      h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right))     flipColors(h);

		h.N = size(h.left) + size(h.right) + 1;
		return h;
	}

	/***************************************************************************
	 *  Metodos utiles del arbol.
	 ***************************************************************************/

	public int height() {
		return height(raiz);
	}

	private int height(NodoArbol x) {
		if (x == null) return -1;
		return 1 + Math.max(height(x.left), height(x.right));
	}

	/***************************************************************************
	 *  Metodos extras del arbol.
	 ***************************************************************************/

	/**
//	 * La llave menor
	 * @return La llave menor.
	 */
	public K min() {
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		return (K) min(raiz).key;
	} 

	private NodoArbol min(NodoArbol x) { 
		if (x.left == null) return x; 
		else return min(x.left); 
	} 

	/**
	 * La llave mayor.
	 * @return La llave mayor
	 */
	public K max(){
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		return (K) max(raiz).key;
	} 

	private NodoArbol max(NodoArbol x) { 
		if (x.right == null) return x; 
		else return max(x.right); 
	} 

	/**
	 * LLave del piso
	 * @param key llave
	 * @return llave del piso
	 */
	public K floor(K key){
		if (key == null) throw new IllegalArgumentException("La llave es null");
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		NodoArbol x = floor(raiz, key);
		if (x == null) return null;
		else           return (K) x.key;
	}  

	private NodoArbol floor(NodoArbol x, K key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp < 0)  return floor(x.left, key);
		NodoArbol t = floor(x.right, key);
		if (t != null) return t; 
		else           return x;
	}

	public K ceiling(K key) {
		if (key == null) throw new IllegalArgumentException("La llave es null");
		if (isEmpty()) throw new NoSuchElementException("El arbol est� vacio");
		NodoArbol x = ceiling(raiz, key);
		if (x == null) return null;
		else           return x.key;  
	}

	private NodoArbol ceiling(NodoArbol x, K key) {  
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp > 0)  return ceiling(x.right, key);
		NodoArbol t = ceiling(x.left, key);
		if (t != null) return t; 
		else           return x;
	}

	public K select(int k) {
		if (k < 0 || k >= size()) {
			throw new IllegalArgumentException("El argumento a seleccionar es invalido: " + k);
		}
		NodoArbol x = select(raiz, k);
		return (K) x.key;
	}

	private NodoArbol select(NodoArbol x, int k) {
		int t = size(x.left); 
		if      (t > k) return select(x.left,  k); 
		else if (t < k) return select(x.right, k-t-1); 
		else            return x; 
	} 




	public int rank(K key) {
		if (key == null) throw new IllegalArgumentException("La llave es null");
		return rank(key, raiz);
	} 

	private int rank(K key, NodoArbol x) {
		if (x == null) return 0; 
		int cmp = key.compareTo((K) x.key); 
		if      (cmp < 0) return rank(key, x.left); 
		else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
		else              return size(x.left); 
	} 

	/***************************************************************************
	 *  Otros.
	 ***************************************************************************/

	public Iterable<K> keys()
	{
		if (isEmpty()) return new Queue<K>();
		return keysInRange(min(), max());
	}
	

	public Iterable<K> keysInRange(K lo, K hi) 
	{
		if (lo == null) throw new IllegalArgumentException("La llave menor es null");
		if (hi == null) throw new IllegalArgumentException("La llave mayor es null");

		Queue<K> queue = new Queue<K>();
		keys(raiz, queue, lo, hi);
		return queue;
	} 

	private void keys(NodoArbol x, Queue<K> queue, K lo, K hi) 
	{ 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	}


	public int size(K lo, K hi)
	{
		if (lo == null) throw new IllegalArgumentException("La llave menor es null");
		if (hi == null) throw new IllegalArgumentException("La llave mayor es null");
		if (lo.compareTo(hi) > 0) return 0;
		if (contains(hi)) return rank(hi) - rank(lo) + 1;
		else              return rank(hi) - rank(lo);
	}

	/***************************************************************************
	 *  Verificar la definicion de un arbol Rojo Negro.
	 ***************************************************************************/
	public boolean check() {
		if (!isBST())            System.out.println("Not in symmetric order");
		if (!isSizeConsistent()) System.out.println("Subtree counts not consistent");
		if (!isRankConsistent()) System.out.println("Ranks not consistent");
		if (!is23())             System.out.println("Not a 2-3 tree");
		if (!isBalanced())       System.out.println("Not balanced");
		return isBST() && isSizeConsistent() && isRankConsistent() && is23() && isBalanced();
	}

	// does this binary tree satisfy symmetric order?
	// Note: this test also ensures that data structure is a binary tree since order is strict
	private boolean isBST() {
		return isBST(raiz, null, null);
	}

	// is the tree rooted at x a BST with all keys strictly between min and max
	// (if min or max is null, treat as empty constraint)
	// Credit: Bob Dondero's elegant solution
	private boolean isBST(NodoArbol x, K min, K max) {
		if (x == null) return true;
		if (min != null && ((Comparable<K>) x.key).compareTo(min) <= 0) return false;
		if (max != null && ((Comparable<K>) x.key).compareTo(max) >= 0) return false;
		return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
	} 

	// are the size fields correct?
	private boolean isSizeConsistent() { return isSizeConsistent(raiz); }
	private boolean isSizeConsistent(NodoArbol x) {
		if (x == null) return true;
		if (x.N != size(x.left) + size(x.right) + 1) return false;
		return isSizeConsistent(x.left) && isSizeConsistent(x.right);
	} 

	// check that ranks are consistent
	private boolean isRankConsistent(){
		for (int i = 0; i < size(); i++)
			if (i != rank(select(i))) return false;
		for (K key : keys())
			if (key.compareTo(select(rank(key))) != 0) return false;
		return true;
	}

	// Does the tree have no red right links, and at most one (left)
	// red links in a row on any path?
	private boolean is23() { return is23(raiz); }
	private boolean is23(NodoArbol x) {
		if (x == null) return true;
		if (isRed(x.right)) return false;
		if (x != raiz && isRed(x) && isRed(x.left))
			return false;
		return is23(x.left) && is23(x.right);
	} 

	// do all paths from root to leaf have same number of black edges?
	private boolean isBalanced() { 
		int black = 0;     // number of black links on path from root to min
		NodoArbol x = raiz;
		while (x != null) {
			if (!isRed(x)) black++;
			x = x.left;
		}
		return isBalanced(raiz, black);
	}

	// does every path from the root to a leaf have the given number of black links?
	private boolean isBalanced(NodoArbol x, int black) {
		if (x == null) return black == 0;
		if (!isRed(x)) black--;
		return isBalanced(x.left, black) && isBalanced(x.right, black);
	}
}
