package model.data_structures;

public interface IQueue<T> extends Iterable<T>
{

	/**
	 * Retorna true si la Cola esta vacia
	 * @return true si la Cola esta vacia, false de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int size();
	
	/**
	 * Inserta un nuevo elemento en la Cola
	 * @param t el nuevo elemento que se va ha agregar
	 */
	public void enqueue(T t);
	
	/**
	 * Quita y retorna el elemento agregado menos recientemente
	 * @return el elemento agregado menos recientemente
	 */
	public T dequeue();
	/**
	 * Retorna el primer elemento de la cola
	 */
	public T darPrimerElemento();
	/**
	 * Une una cola con otra (ultimo elemento se une con el primer elemento de la cola pasada por parametro)
	 */
	public IQueue<T> unirColas(IQueue<T> colaAunir);
	/**
	 * Da el primer elemento de una cola
	 */
	public Node<T> darPrimero();
	/**
	 * Da el ultimo elemento de una cola
	 */
	public Node<T> darUltimo();
	/**
	 * Permite definir el elemento primero de una cola
	 */
	public void setPrimero(Node<T> pPrimero);
	/**
	 * Permite definir el elemento final de una cola
	 */
	public void setUltimo(Node<T> pUltimo);
	/**
	 * Da el tama�o de la cola
	 */
	public int darTamano();
	/**
	 * Permite definir el tama�o de una cola
	 */
	public void setTamano(int pTamano);
}
