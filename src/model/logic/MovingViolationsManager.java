package model.logic;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.sun.xml.internal.ws.api.pipe.NextAction;

import model.data_structures.*;
import model.util.Sort;
import model.vo.*;
/**
 * Clase que administra administradora de infracciones 
 * @author nicot
 */
public class MovingViolationsManager 
{

	//TODO Definir atributos necesarios
	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[12];
	/**
	 * numero total de infracciones
	 */
	private int numTotalInfrac;
	/**
	 * Cola que guarda las infracciones por Fecha
	 */
	private MaxHeapCP<VOMovingViolations> colaPrioridadFecha;
	/**
	 * Tabla que guarda las todas las infracciones por violation code.
	 */
	private HashSeparateChaining<String,Queue<VOMovingViolations>> TablaViolationCode; 
	/**
	 * Tabla que guarda todas las infracciones por coordenadas.
	 */
	private HashSeparateChaining<Double,Queue<VOMovingViolations>> TablaUbicacion;
	/**
	 * Tabla que guarda todas las infracciones por address_Id.
	 */
	private HashSeparateChaining<String,Queue<VOMovingViolations>> TablaAddress;
	/**
	 * Arbol black and red que guarda las infraccion por su fecha
	 */
	private ArbolRojoNegro<String,Queue<VOMovingViolations>> arbolFechas;
	/**
	 *  Arbol black and red que guarda las infraccion por su ticketIsuedates
	 */
	private ArbolRojoNegro<String,Queue<VOMovingViolations>> arbolHoras;

	/**
	 * Arbol que guarda todas las infracciones por CordVO.
	 */
	private ArbolRojoNegro<CordVO, Queue<VOMovingViolations>> ArbolUbicacion;

	/**
	 * Metodo constructor
	 * @param semestre Sememestre a cargar 
	 */
	public MovingViolationsManager()
	{
		//TODO inicializar los atributos

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="April";
		listaMes[4]="May";
		listaMes[5]="June";
		listaMes[6]="July";
		listaMes[7]="August";
		listaMes[8]="September";
		listaMes[9]="October";
		listaMes[10]="November";
		listaMes[11]="December";

	}

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public EstadisticasCargaInfracciones loadMovingViolations(int numeroSemestre) 
	{
		// TODO Realizar la carga de infracciones del semestre
		colaPrioridadFecha=new MaxHeapCP<VOMovingViolations>(1000000);
		TablaViolationCode=new HashSeparateChaining<String, Queue<VOMovingViolations>>(100000);
		TablaUbicacion=new HashSeparateChaining<Double, Queue<VOMovingViolations>>(100000);
		TablaAddress=new HashSeparateChaining<String, Queue<VOMovingViolations>>(100000);

		ArbolUbicacion = new ArbolRojoNegro<CordVO,Queue<VOMovingViolations>>();

		arbolFechas=new ArbolRojoNegro<String, Queue<VOMovingViolations>>();
		arbolHoras=new ArbolRojoNegro<String, Queue<VOMovingViolations>>();


		numTotalInfrac=0;

		//Elimina todo rastro de las estructuras anterior
		Runtime garbage = Runtime.getRuntime();
		garbage.gc();

		//Variables para adquirir la informacion para la clase de estadisticas
		Double minX=Double.MAX_VALUE;
		Double minY=Double.MAX_VALUE;
		Double maxX=Double.MIN_VALUE;
		Double maxY=Double.MIN_VALUE;  		
		int numMesesCargados=0;
		int infracPorMes[]=new int[6];
		double maxCordMinCord[]=new double[4];

		try 
		{
			int x;
			int y=0;
			x=numeroSemestre==1?0:6;
			int numTotalInfracciones=0;

			for (int i = 0; i < 6; i++) 
			{
				String xMes=listaMes[x];
				String nombreMes=xMes;
				CSVReader csvReader = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_"+nombreMes+"_2018.csv"));
				String[] fila=null;
				csvReader.readNext();
				int numInfrMes=0;

				while ((fila=csvReader.readNext()) != null) 
				{
					VOMovingViolations infraccion;
					//Crea la infraccion.
					if(xMes.equals("October")|xMes.equals("November")|xMes.equals("December"))
					{
						infraccion=new VOMovingViolations(fila[0], fila[16], fila[2], fila[2], fila[12],ManejoFechaHora.convertirFecha_Hora_LDT(fila[14]), fila[15], fila[8], fila[4],fila[3], fila[10],fila[11],fila[5],fila[6]);
					}
					else 
					{
						infraccion=new VOMovingViolations(fila[0], fila[15], fila[2],fila[9], fila[12], ManejoFechaHora.convertirFecha_Hora_LDT(fila[13]),fila[14],fila[8], fila[4],fila[3], fila[10],fila[11],fila[5],fila[6]);
					}
					//La agrega a la pila
					numInfrMes++;

					//Carga las infracciones en la cola de prioridad
					colaPrioridadFecha.agregar(infraccion);

					//Tabla de hash con las infracciones por codigo de infraccion

					Queue<VOMovingViolations> infraccionesCodigo=null;

					String key=infraccion.getViolationCode();

					if(TablaViolationCode.contains(key))
					{
						infraccionesCodigo=TablaViolationCode.get(key);
						infraccionesCodigo.enqueue(infraccion);
						TablaViolationCode.put(key, infraccionesCodigo);
					}
					else
					{
						infraccionesCodigo=new Queue<VOMovingViolations>();
						infraccionesCodigo.enqueue(infraccion);
						TablaViolationCode.put(key, infraccionesCodigo);
					}

					//Tabla de hash con las ubicaciones por coordenada.

					Queue<VOMovingViolations> ubicaciones=null;

					Double key2=(infraccion.getXcord()%2)+(infraccion.getYcord()%2);

					if(TablaUbicacion.contains(key2))
					{
						ubicaciones=TablaUbicacion.get(key2);
						ubicaciones.enqueue(infraccion);
						TablaUbicacion.put(key2,ubicaciones);
					}
					else
					{
						ubicaciones=new Queue<VOMovingViolations>();
						ubicaciones.enqueue(infraccion);
						TablaUbicacion.put(key2,ubicaciones);
					}

					//Tabla de hash con las infracciones por addressId

					Queue<VOMovingViolations> infraccionesAddress=null;

					String key3=infraccion.getAddressId();

					if(TablaAddress.contains(key3))
					{
						infraccionesAddress=TablaAddress.get(key3);
						infraccionesAddress.enqueue(infraccion);
						TablaAddress.put(key3, infraccionesAddress);
					}
					else
					{
						infraccionesAddress=new Queue<VOMovingViolations>();
						infraccionesAddress.enqueue(infraccion);
						TablaAddress.put(key3, infraccionesAddress);
					}

					//Arbol black-red por fechas

					Queue<VOMovingViolations> infraccionesArbolFechas=null;

					String fechaTxt=infraccion.getTicketIssueDate().toString().split("T")[0];
					LocalDate fecha=ManejoFechaHora.convertirFecha_LD(fechaTxt);
					String key4=fecha.toString();

					if(arbolFechas.contains(key4))
					{
						infraccionesArbolFechas=arbolFechas.get(key4);
						infraccionesArbolFechas.enqueue(infraccion);
						arbolFechas.put(key4, infraccionesArbolFechas);
					}
					else
					{
						infraccionesArbolFechas=new Queue<VOMovingViolations>();
						infraccionesArbolFechas.enqueue(infraccion);
						arbolFechas.put(key4, infraccionesArbolFechas);
					}

					//Arbol black-red por horas

					Queue<VOMovingViolations> infraccionesArbolHoras=null;

					String horaTxt=infraccion.getTicketIssueDate().toString().split("T")[1];
					horaTxt=horaTxt+":00";
					LocalTime hora=ManejoFechaHora.convertirHora_LT(horaTxt);
					String key5=hora.toString()+":00";

					if(arbolHoras.contains(key5))
					{
						infraccionesArbolHoras=arbolHoras.get(key5);
						infraccionesArbolHoras.enqueue(infraccion);
						arbolHoras.put(key5, infraccionesArbolHoras);
					}
					else
					{
						infraccionesArbolHoras=new Queue<VOMovingViolations>();
						infraccionesArbolHoras.enqueue(infraccion);
						arbolHoras.put(key5, infraccionesArbolHoras);
					}

					//Arbol por Ubicacion

					Queue<VOMovingViolations> infraccionesUbicacion = null;

					CordVO key6 = new CordVO(infraccion.getXcord(), infraccion.getYcord());

					if(ArbolUbicacion.contains(key6))
					{
						infraccionesUbicacion = ArbolUbicacion.get(key6);
						infraccionesUbicacion.enqueue(infraccion);
						ArbolUbicacion.put(key6, infraccionesUbicacion);
					}
					else
					{
						infraccionesUbicacion = new Queue<VOMovingViolations>();
						infraccionesUbicacion.enqueue(infraccion);
						ArbolUbicacion.put(key6, infraccionesUbicacion);
						//Consigue la coordeenada minima y maxima de la zona geografica.
					}


					if(maxX<infraccion.getXcord())
						maxX=infraccion.getXcord();
					if(maxY<infraccion.getYcord())
						maxY=infraccion.getYcord();
					if(minY>infraccion.getYcord())
						minY=infraccion.getYcord();
					if(minX>infraccion.getXcord())
						minX=infraccion.getXcord();
				}
				numTotalInfracciones+=numInfrMes;
				infracPorMes[y]=numInfrMes;
				y++;
				x++;
				numMesesCargados++;
				csvReader.close();
			} 


			maxCordMinCord[0]=minX;
			maxCordMinCord[1]=minY;
			maxCordMinCord[2]=maxX;
			maxCordMinCord[3]=maxY;

			numTotalInfrac=numTotalInfracciones;
			//Crea la clase de estadisticas
			EstadisticasCargaInfracciones infoCarga=new EstadisticasCargaInfracciones(numTotalInfrac,numMesesCargados, infracPorMes, maxCordMinCord);

			return infoCarga;
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	 * que tengan m�s infracciones. 
	 * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	 * @return Cola con objetos InfraccionesFranjaHoraria
	 */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		// TODO completar
		//Cola con objetos de respuesta
		IQueue<InfraccionesFranjaHoraria> colaRta=new Queue<InfraccionesFranjaHoraria>();
		//MaxHeap con infracciones organizadas por fecha.
		MaxHeapCP<VOMovingViolations> heapInfracciones=new MaxHeapCP<VOMovingViolations>(1000000);
		ArregloDinamico<VOMovingViolations> arregloOriginal=colaPrioridadFecha.darArreglo();
		heapInfracciones.setArreglo(arregloOriginal,arregloOriginal.darTamano());


		int numElementos=heapInfracciones.darNumElementos();

		//Colas que albergan las infracciones, organizadas por intervalo
		Queue<VOMovingViolations> colaFranja0t1=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja1t2=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja2t3=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja3t4=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja4t5=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja5t6=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja6t7=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja7t8=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja8t9=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja9t10=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja10t11=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja11t12=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja12t13=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja13t14=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja14t15=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja15t16=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja16t17=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja17t18=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja18t19=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja19t20=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja20t21=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja21t22=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja22t23=new  Queue<VOMovingViolations>();
		Queue<VOMovingViolations> colaFranja23t24=new  Queue<VOMovingViolations>();

		//Ciclo que llena las colas por su franja de hora
		for (int i = 0; i < numElementos; i++) 
		{
			VOMovingViolations x=heapInfracciones.delMax();
			LocalDateTime fecha=x.getTicketIssueDate();
			LocalTime hora=fecha.toLocalTime();
			if((hora.isAfter(LocalTime.parse("00:00"))&&hora.isBefore(LocalTime.parse("00:59")))||hora.equals(LocalTime.parse("00:00")))
			{
				colaFranja0t1.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("01:00"))&&hora.isBefore(LocalTime.parse("01:59")))||hora.equals(LocalTime.parse("01:00")))
			{
				colaFranja1t2.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("02:00"))&&hora.isBefore(LocalTime.parse("02:59")))||hora.equals(LocalTime.parse("02:00")))
			{
				colaFranja2t3.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("03:00"))&&hora.isBefore(LocalTime.parse("03:59")))||hora.equals(LocalTime.parse("03:00")))
			{
				colaFranja3t4.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("04:00"))&&hora.isBefore(LocalTime.parse("04:59")))||hora.equals(LocalTime.parse("04:00")))
			{
				colaFranja4t5.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("05:00"))&&hora.isBefore(LocalTime.parse("05:59")))||hora.equals(LocalTime.parse("05:00")))
			{
				colaFranja5t6.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("06:00"))&&hora.isBefore(LocalTime.parse("06:59")))||hora.equals(LocalTime.parse("06:00")))
			{
				colaFranja6t7.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("07:00"))&&hora.isBefore(LocalTime.parse("07:59")))||hora.equals(LocalTime.parse("07:00")))
			{
				colaFranja7t8.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("08:00"))&&hora.isBefore(LocalTime.parse("08:59")))||hora.equals(LocalTime.parse("08:00")))
			{
				colaFranja8t9.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("09:00"))&&hora.isBefore(LocalTime.parse("09:59")))||hora.equals(LocalTime.parse("09:00")))
			{
				colaFranja9t10.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("10:00"))&&hora.isBefore(LocalTime.parse("10:59")))||hora.equals(LocalTime.parse("10:00")))
			{
				colaFranja10t11.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("11:00"))&&hora.isBefore(LocalTime.parse("11:59")))||hora.equals(LocalTime.parse("11:00")))
			{
				colaFranja11t12.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("12:00"))&&hora.isBefore(LocalTime.parse("12:59")))||hora.equals(LocalTime.parse("12:00")))
			{
				colaFranja12t13.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("13:00"))&&hora.isBefore(LocalTime.parse("13:59")))||hora.equals(LocalTime.parse("13:00")))
			{
				colaFranja13t14.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("14:00"))&&hora.isBefore(LocalTime.parse("14:59")))||hora.equals(LocalTime.parse("14:00")))
			{
				colaFranja14t15.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("15:00"))&&hora.isBefore(LocalTime.parse("15:59")))||hora.equals(LocalTime.parse("15:00")))
			{
				colaFranja15t16.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("16:00"))&&hora.isBefore(LocalTime.parse("16:59")))||hora.equals(LocalTime.parse("16:00")))
			{
				colaFranja16t17.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("17:00"))&&hora.isBefore(LocalTime.parse("17:59")))||hora.equals(LocalTime.parse("17:00")))
			{
				colaFranja17t18.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("18:00"))&&hora.isBefore(LocalTime.parse("18:59")))||hora.equals(LocalTime.parse("18:00")))
			{
				colaFranja18t19.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("19:00"))&&hora.isBefore(LocalTime.parse("19:59")))||hora.equals(LocalTime.parse("19:00")))
			{
				colaFranja19t20.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("20:00"))&&hora.isBefore(LocalTime.parse("20:59")))||hora.equals(LocalTime.parse("20:00")))
			{
				colaFranja20t21.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("21:00"))&&hora.isBefore(LocalTime.parse("21:59")))||hora.equals(LocalTime.parse("21:00")))
			{
				colaFranja21t22.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("22:00"))&&hora.isBefore(LocalTime.parse("22:59")))||hora.equals(LocalTime.parse("22:00")))
			{
				colaFranja22t23.enqueue(x);
			}
			else if((hora.isAfter(LocalTime.parse("23:00"))&&hora.isBefore(LocalTime.parse("23:59")))||hora.equals(LocalTime.parse("23:00")))
			{
				colaFranja23t24.enqueue(x);
			}
		}

		//Realiza un ciclo para determinar cual cola es la mayor y las va insertando.
		Queue<VOMovingViolations> tmp=null;
		LocalTime horaInicio=null;
		LocalTime horaFinal=null;
		for (int i = 0; i < N; i++) 
		{
			int numMaxCola=Integer.MIN_VALUE;

			if(colaFranja0t1!=null&&colaFranja0t1.size()>numMaxCola)
			{
				tmp=colaFranja0t1;
				numMaxCola=colaFranja0t1.size();
				horaInicio=LocalTime.parse("00:00");
				horaFinal=LocalTime.parse("00:59");
			}
			if(colaFranja1t2!=null&&colaFranja1t2.size()>numMaxCola)
			{
				tmp=colaFranja1t2;
				numMaxCola=colaFranja1t2.size();
				horaInicio=LocalTime.parse("01:00");
				horaFinal=LocalTime.parse("01:59");
			}
			if(colaFranja2t3!=null&&colaFranja2t3.size()>numMaxCola)
			{
				tmp=colaFranja2t3;
				numMaxCola=colaFranja2t3.size();
				horaInicio=LocalTime.parse("02:00");
				horaFinal=LocalTime.parse("02:59");
			}
			if(colaFranja3t4!=null&&colaFranja3t4.size()>numMaxCola)
			{
				tmp=colaFranja3t4;
				numMaxCola=colaFranja3t4.size();
				horaInicio=LocalTime.parse("03:00");
				horaFinal=LocalTime.parse("03:59");
			}
			if(colaFranja4t5!=null&&colaFranja4t5.size()>numMaxCola)
			{
				tmp=colaFranja4t5;
				numMaxCola=colaFranja4t5.size();
				horaInicio=LocalTime.parse("04:00");
				horaFinal=LocalTime.parse("04:59");
			}
			if(colaFranja5t6!=null&&colaFranja5t6.size()>numMaxCola)
			{
				tmp=colaFranja5t6;
				numMaxCola=colaFranja5t6.size();
				horaInicio=LocalTime.parse("05:00");
				horaFinal=LocalTime.parse("05:59");
			}
			if(colaFranja6t7!=null&&colaFranja6t7.size()>numMaxCola)
			{
				tmp=colaFranja6t7;
				numMaxCola=colaFranja6t7.size();
				horaInicio=LocalTime.parse("06:00");
				horaFinal=LocalTime.parse("06:59");
			}
			if(colaFranja7t8!=null&&colaFranja7t8.size()>numMaxCola)
			{
				tmp=colaFranja7t8;
				numMaxCola=colaFranja7t8.size();
				horaInicio=LocalTime.parse("07:00");
				horaFinal=LocalTime.parse("07:59");
			}
			if(colaFranja8t9!=null&&colaFranja8t9.size()>numMaxCola)
			{
				tmp=colaFranja8t9;
				numMaxCola=colaFranja8t9.size();
				horaInicio=LocalTime.parse("08:00");
				horaFinal=LocalTime.parse("08:59");
			}
			if(colaFranja9t10!=null&&colaFranja9t10.size()>numMaxCola)
			{
				tmp=colaFranja9t10;
				numMaxCola=colaFranja9t10.size();
				horaInicio=LocalTime.parse("09:00");
				horaFinal=LocalTime.parse("09:59");
			}
			if(colaFranja10t11!=null&&colaFranja10t11.size()>numMaxCola)
			{
				tmp=colaFranja10t11;
				numMaxCola=colaFranja10t11.size();
				horaInicio=LocalTime.parse("10:00");
				horaFinal=LocalTime.parse("10:59");
			}
			if(colaFranja11t12!=null&&colaFranja11t12.size()>numMaxCola)
			{
				tmp=colaFranja11t12;
				numMaxCola=colaFranja11t12.size();
				horaInicio=LocalTime.parse("11:00");
				horaFinal=LocalTime.parse("11:59");
			}
			if(colaFranja12t13!=null&&colaFranja12t13.size()>numMaxCola)
			{
				tmp=colaFranja12t13;
				numMaxCola=colaFranja12t13.size();
				horaInicio=LocalTime.parse("12:00");
				horaFinal=LocalTime.parse("12:59");
			}
			if(colaFranja13t14!=null&&colaFranja13t14.size()>numMaxCola)
			{
				tmp=colaFranja13t14;
				numMaxCola=colaFranja13t14.size();
				horaInicio=LocalTime.parse("13:00");
				horaFinal=LocalTime.parse("13:59");
			}
			if(colaFranja14t15!=null&&colaFranja14t15.size()>numMaxCola)
			{
				tmp=colaFranja14t15;
				numMaxCola=colaFranja14t15.size();
				horaInicio=LocalTime.parse("14:00");
				horaFinal=LocalTime.parse("14:59");
			}
			if(colaFranja15t16!=null&&colaFranja15t16.size()>numMaxCola)
			{
				tmp=colaFranja15t16;
				numMaxCola=colaFranja15t16.size();
				horaInicio=LocalTime.parse("15:00");
				horaFinal=LocalTime.parse("15:59");
			}
			if(colaFranja16t17!=null&&colaFranja16t17.size()>numMaxCola)
			{
				tmp=colaFranja16t17;
				numMaxCola=colaFranja16t17.size();
				horaInicio=LocalTime.parse("16:00");
				horaFinal=LocalTime.parse("16:59");
			}
			if(colaFranja17t18!=null&&colaFranja17t18.size()>numMaxCola)
			{
				tmp=colaFranja17t18;
				numMaxCola=colaFranja17t18.size();
				horaInicio=LocalTime.parse("17:00");
				horaFinal=LocalTime.parse("17:59");
			}
			if(colaFranja18t19!=null&&colaFranja18t19.size()>numMaxCola)
			{
				tmp=colaFranja18t19;
				numMaxCola=colaFranja18t19.size();
				horaInicio=LocalTime.parse("18:00");
				horaFinal=LocalTime.parse("18:59");
			}
			if(colaFranja19t20!=null&&colaFranja19t20.size()>numMaxCola)
			{
				tmp=colaFranja19t20;
				numMaxCola=colaFranja19t20.size();
				horaInicio=LocalTime.parse("19:00");
				horaFinal=LocalTime.parse("19:59");
			}
			if(colaFranja20t21!=null&&colaFranja20t21.size()>numMaxCola)
			{
				tmp=colaFranja20t21;
				numMaxCola=colaFranja20t21.size();
				horaInicio=LocalTime.parse("20:00");
				horaFinal=LocalTime.parse("20:59");
			}
			if(colaFranja21t22!=null&&colaFranja21t22.size()>numMaxCola)
			{
				tmp=colaFranja21t22;
				numMaxCola=colaFranja21t22.size();
				horaInicio=LocalTime.parse("21:00");
				horaFinal=LocalTime.parse("21:59");
			}
			if(colaFranja22t23!=null&&colaFranja22t23.size()>numMaxCola)
			{
				tmp=colaFranja22t23;
				numMaxCola=colaFranja22t23.size();
				horaInicio=LocalTime.parse("22:00");
				horaFinal=LocalTime.parse("22:59");
			}
			if(colaFranja23t24!=null&&colaFranja22t23.size()>numMaxCola)
			{
				tmp=colaFranja23t24;
				numMaxCola=colaFranja23t24.size();
				horaInicio=LocalTime.parse("23:00");
				horaFinal=LocalTime.parse("23:59");
			}

			InfraccionesFranjaHoraria ifh=new InfraccionesFranjaHoraria(horaInicio, horaFinal, tmp);
			colaRta.enqueue(ifh);

			if(tmp==colaFranja0t1)
				colaFranja0t1=null;
			else if(tmp==colaFranja1t2)
				colaFranja1t2=null;
			else if(tmp==colaFranja2t3)
				colaFranja2t3=null;
			else if(tmp==colaFranja3t4)
				colaFranja3t4=null;
			else if(tmp==colaFranja4t5)
				colaFranja4t5=null;
			else if(tmp==colaFranja5t6)
				colaFranja5t6=null;
			else if(tmp==colaFranja6t7)
				colaFranja6t7=null;
			else if(tmp==colaFranja7t8)
				colaFranja7t8=null;
			else if(tmp==colaFranja8t9)
				colaFranja8t9=null;
			else if(tmp==colaFranja10t11)
				colaFranja10t11=null;
			else if(tmp==colaFranja11t12)
				colaFranja11t12=null;
			else if(tmp==colaFranja12t13)
				colaFranja12t13=null;
			else if(tmp==colaFranja13t14)
				colaFranja13t14=null;
			else if(tmp==colaFranja14t15)
				colaFranja14t15=null;
			else if(tmp==colaFranja15t16)
				colaFranja15t16=null;
			else if(tmp==colaFranja16t17)
				colaFranja16t17=null;
			else if(tmp==colaFranja17t18)
				colaFranja17t18=null;
			else if(tmp==colaFranja18t19)
				colaFranja18t19=null;
			else if(tmp==colaFranja20t21)
				colaFranja20t21=null;
			else if(tmp==colaFranja21t22)
				colaFranja21t22=null;
			else if(tmp==colaFranja22t23)
				colaFranja22t23=null;
			else if(tmp==colaFranja23t24)
				colaFranja23t24=null;
			tmp=null;
			horaInicio=null;
			horaFinal=null;
		}	
		return colaRta;		
	}

	/**
	 * Requerimiento 2A: Consultar  las  infracciones  por
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{
		// TODO completar
		//Consigue la cola de infracciones y las organiza por medio de quickSort
		Double key=(xCoord%2)+(yCoord%2);

		Queue<VOMovingViolations> infraccionesCord=TablaUbicacion.get(key);

		//La coordenada ingresada no existe dentro de la tabla.
		if(infraccionesCord==null)
		{
			System.out.println("La coordenada ingresada no existe");
			return null;
		}

		VOMovingViolations primeraInfraccion=infraccionesCord.darPrimerElemento();

		int address = 0;
		if(primeraInfraccion.getAddressId() != null && !primeraInfraccion.getAddressId().equals("")) address=Integer.parseInt(primeraInfraccion.getAddressId());
		String location=primeraInfraccion.getLocation();
		int street = 0;
		if(primeraInfraccion.getStreetSegId() != null && !primeraInfraccion.getStreetSegId().equals("")) street=Integer.parseInt(primeraInfraccion.getStreetSegId());

		//Crea el objeto para retornar
		InfraccionesLocalizacion rta=new InfraccionesLocalizacion(xCoord, yCoord, location, address, street, infraccionesCord);

		return rta;		
	}


	/**
	 * Requerimiento 3A: Buscar las infracciones por rango de fechas
	 * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	 * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	 * @return Cola con objetos InfraccionesFecha
	 */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		// TODO completar

		IQueue<InfraccionesFecha> colaRta=new Queue<InfraccionesFecha>();

		String sfechaInicial=fechaInicial.toString();
		String sfechaFinal=fechaFinal.toString();

		Iterable<String> x= arbolFechas.keysInRange(sfechaInicial,sfechaFinal);    
		IteratorQueue<String> it= (IteratorQueue<String>)x.iterator();

		while(it.hasNext())
		{
			String fecha=it.next();
			LocalDate fechaY=LocalDate.parse(fecha);
			IQueue<VOMovingViolations> listaFecha=arbolFechas.get(fecha);
			InfraccionesFecha z=new InfraccionesFecha(listaFecha, fechaY);
			colaRta.enqueue(z);
		}


		return colaRta;		
	}

	/**
	 * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	 * (ViolationCode)  que  tengan  m�s infracciones.
	 * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	 * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	 */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		// TODO completar
		IQueue<InfraccionesViolationCode> cola = new Queue<InfraccionesViolationCode>();
		Iterator<String> keys =TablaViolationCode.keys();
		MaxHeapCP<InfraccionesViolationCode> colaPrioridad = new MaxHeapCP<InfraccionesViolationCode>(1000000);
		while(keys.hasNext()) 
		{
			String key = (String) keys.next();			
			InfraccionesViolationCode VC = new InfraccionesViolationCode(key, TablaViolationCode.get(key));
			colaPrioridad.agregar(VC);
		}
		MaxHeapCP<InfraccionesViolationCode> copiaCP = colaPrioridad;
		for(int i = 0; i < N; i++) 
		{
			cola.enqueue(copiaCP.delMax());
		}
		return cola;		
	}


	/**
	 * Requerimiento 2B: Consultar las  infracciones  por  
	 * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	 * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	 *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord)
	{
		// TODO completar
		Queue<VOMovingViolations> colaP = ArbolUbicacion.get(new CordVO(xCoord, yCoord));
		if(colaP == null) {
			System.out.println("La coordenada ingresada no existe");
			return null;
		}
		VOMovingViolations VOMV = colaP.darPrimerElemento();
		InfraccionesLocalizacion IL = new InfraccionesLocalizacion(xCoord, yCoord, VOMV.getLocation(), Integer.parseInt(VOMV.getAddressId()), Integer.parseInt(VOMV.getStreetSegId()), colaP);
		return IL;		
	}

	/**
	 * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	 * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	 * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	 * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	 * @return Cola con objetos InfraccionesFechaHora
	 */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{
		// TODO completar
		//Cola con objetos de respuesta
		IQueue<InfraccionesFechaHora> colaRta=new Queue<InfraccionesFechaHora>();
		ArbolRojoNegro<String,Queue<VOMovingViolations>> arbolFechaRango=new ArbolRojoNegro<String, Queue<VOMovingViolations>>();
		ArbolRojoNegro<Double,InfraccionesFechaHora> arbolReq=new ArbolRojoNegro<Double,InfraccionesFechaHora>();

		//Se usa arbolFechas porque por fecha tengo todas las infracciones (sin importar su hora)
		MaxHeapCP<VOMovingViolations> colaPtmp=new MaxHeapCP<VOMovingViolations>(1000000);
		ArregloDinamico<VOMovingViolations> arregloOriginal=colaPrioridadFecha.darArreglo();
		colaPtmp.setArreglo(arregloOriginal, arregloOriginal.darTamano());

		for (int i = 0; i < colaPtmp.darNumElementos(); i++) 
		{
			VOMovingViolations infraccion=colaPtmp.delMax();
			LocalDateTime fecha=infraccion.getTicketIssueDate();
			LocalTime hora=fecha.toLocalTime();
			if((hora.isAfter(LocalTime.parse("00:00"))&&hora.isBefore(LocalTime.parse("00:59")))||hora.equals(LocalTime.parse("00:00")))
			{
				Queue<VOMovingViolations> infraccionesRango01=null;

				String key=fecha.toLocalDate()+"T00";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango01=arbolFechaRango.get(key);
					infraccionesRango01.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango01);
				}
				else
				{
					infraccionesRango01=new Queue<VOMovingViolations>();
					infraccionesRango01.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango01);
				}
			}
			else if((hora.isAfter(LocalTime.parse("01:00"))&&hora.isBefore(LocalTime.parse("01:59")))||hora.equals(LocalTime.parse("01:00")))
			{
				Queue<VOMovingViolations> infraccionesRango02=null;

				String key=fecha.toLocalDate()+"T01";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango02=arbolFechaRango.get(key);
					infraccionesRango02.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango02);
				}
				else
				{
					infraccionesRango02=new Queue<VOMovingViolations>();
					infraccionesRango02.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango02);
				}
			}
			else if((hora.isAfter(LocalTime.parse("02:00"))&&hora.isBefore(LocalTime.parse("02:59")))||hora.equals(LocalTime.parse("02:00")))
			{
				Queue<VOMovingViolations> infraccionesRango03=null;

				String key=fecha.toLocalDate()+"T02";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango03=arbolFechaRango.get(key);
					infraccionesRango03.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango03);
				}
				else
				{
					infraccionesRango03=new Queue<VOMovingViolations>();
					infraccionesRango03.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango03);
				}
			}
			else if((hora.isAfter(LocalTime.parse("03:00"))&&hora.isBefore(LocalTime.parse("03:59")))||hora.equals(LocalTime.parse("03:00")))
			{
				Queue<VOMovingViolations> infraccionesRango04=null;

				String key=fecha.toLocalDate()+"T03";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango04=arbolFechaRango.get(key);
					infraccionesRango04.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango04);
				}
				else
				{
					infraccionesRango04=new Queue<VOMovingViolations>();
					infraccionesRango04.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango04);
				}
			}
			else if((hora.isAfter(LocalTime.parse("04:00"))&&hora.isBefore(LocalTime.parse("04:59")))||hora.equals(LocalTime.parse("04:00")))
			{
				Queue<VOMovingViolations> infraccionesRango05=null;

				String key=fecha.toLocalDate()+"T04";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango05=arbolFechaRango.get(key);
					infraccionesRango05.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango05);
				}
				else
				{
					infraccionesRango05=new Queue<VOMovingViolations>();
					infraccionesRango05.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango05);
				}
			}
			else if((hora.isAfter(LocalTime.parse("05:00"))&&hora.isBefore(LocalTime.parse("05:59")))||hora.equals(LocalTime.parse("05:00")))
			{
				Queue<VOMovingViolations> infraccionesRango06=null;

				String key=fecha.toLocalDate()+"T05";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango06=arbolFechaRango.get(key);
					infraccionesRango06.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango06);
				}
				else
				{
					infraccionesRango06=new Queue<VOMovingViolations>();
					infraccionesRango06.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango06);
				}
			}
			else if((hora.isAfter(LocalTime.parse("06:00"))&&hora.isBefore(LocalTime.parse("06:59")))||hora.equals(LocalTime.parse("06:00")))
			{
				Queue<VOMovingViolations> infraccionesRango07=null;

				String key=fecha.toLocalDate()+"T06";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango07=arbolFechaRango.get(key);
					infraccionesRango07.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango07);
				}
				else
				{
					infraccionesRango07=new Queue<VOMovingViolations>();
					infraccionesRango07.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango07);
				}
			}
			else if((hora.isAfter(LocalTime.parse("07:00"))&&hora.isBefore(LocalTime.parse("07:59")))||hora.equals(LocalTime.parse("07:00")))
			{
				Queue<VOMovingViolations> infraccionesRango08=null;

				String key=fecha.toLocalDate()+"T07";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango08=arbolFechaRango.get(key);
					infraccionesRango08.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango08);
				}
				else
				{
					infraccionesRango08=new Queue<VOMovingViolations>();
					infraccionesRango08.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango08);
				}
			}
			else if((hora.isAfter(LocalTime.parse("08:00"))&&hora.isBefore(LocalTime.parse("08:59")))||hora.equals(LocalTime.parse("08:00")))
			{
				Queue<VOMovingViolations> infraccionesRango09=null;

				String key=fecha.toLocalDate()+"T08";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango09=arbolFechaRango.get(key);
					infraccionesRango09.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango09);
				}
				else
				{
					infraccionesRango09=new Queue<VOMovingViolations>();
					infraccionesRango09.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango09);
				}
			}
			else if((hora.isAfter(LocalTime.parse("09:00"))&&hora.isBefore(LocalTime.parse("09:59")))||hora.equals(LocalTime.parse("09:00")))
			{
				Queue<VOMovingViolations> infraccionesRango10=null;

				String key=fecha.toLocalDate()+"T09";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango10=arbolFechaRango.get(key);
					infraccionesRango10.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango10);
				}
				else
				{
					infraccionesRango10=new Queue<VOMovingViolations>();
					infraccionesRango10.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango10);
				}
			}
			else if((hora.isAfter(LocalTime.parse("10:00"))&&hora.isBefore(LocalTime.parse("10:59")))||hora.equals(LocalTime.parse("10:00")))
			{
				Queue<VOMovingViolations> infraccionesRango11=null;

				String key=fecha.toLocalDate()+"T10";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango11=arbolFechaRango.get(key);
					infraccionesRango11.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango11);
				}
				else
				{
					infraccionesRango11=new Queue<VOMovingViolations>();
					infraccionesRango11.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango11);
				}
			}
			else if((hora.isAfter(LocalTime.parse("11:00"))&&hora.isBefore(LocalTime.parse("11:59")))||hora.equals(LocalTime.parse("11:00")))
			{
				Queue<VOMovingViolations> infraccionesRango12=null;

				String key=fecha.toLocalDate()+"T11";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango12=arbolFechaRango.get(key);
					infraccionesRango12.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango12);
				}
				else
				{
					infraccionesRango12=new Queue<VOMovingViolations>();
					infraccionesRango12.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango12);
				}
			}
			else if((hora.isAfter(LocalTime.parse("12:00"))&&hora.isBefore(LocalTime.parse("12:59")))||hora.equals(LocalTime.parse("12:00")))
			{
				Queue<VOMovingViolations> infraccionesRango13=null;

				String key=fecha.toLocalDate()+"T12";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango13=arbolFechaRango.get(key);
					infraccionesRango13.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango13);
				}
				else
				{
					infraccionesRango13=new Queue<VOMovingViolations>();
					infraccionesRango13.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango13);
				}
			}
			else if((hora.isAfter(LocalTime.parse("13:00"))&&hora.isBefore(LocalTime.parse("13:59")))||hora.equals(LocalTime.parse("13:00")))
			{
				Queue<VOMovingViolations> infraccionesRango14=null;

				String key=fecha.toLocalDate()+"T13";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango14=arbolFechaRango.get(key);
					infraccionesRango14.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango14);
				}
				else
				{
					infraccionesRango14=new Queue<VOMovingViolations>();
					infraccionesRango14.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango14);
				}
			}
			else if((hora.isAfter(LocalTime.parse("14:00"))&&hora.isBefore(LocalTime.parse("14:59")))||hora.equals(LocalTime.parse("14:00")))
			{
				Queue<VOMovingViolations> infraccionesRango15=null;

				String key=fecha.toLocalDate()+"T14";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango15=arbolFechaRango.get(key);
					infraccionesRango15.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango15);
				}
				else
				{
					infraccionesRango15=new Queue<VOMovingViolations>();
					infraccionesRango15.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango15);
				}
			}
			else if((hora.isAfter(LocalTime.parse("15:00"))&&hora.isBefore(LocalTime.parse("15:59")))||hora.equals(LocalTime.parse("15:00")))
			{
				Queue<VOMovingViolations> infraccionesRango16=null;

				String key=fecha.toLocalDate()+"T15";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango16=arbolFechaRango.get(key);
					infraccionesRango16.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango16);
				}
				else
				{
					infraccionesRango16=new Queue<VOMovingViolations>();
					infraccionesRango16.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango16);
				}
			}
			else if((hora.isAfter(LocalTime.parse("16:00"))&&hora.isBefore(LocalTime.parse("16:59")))||hora.equals(LocalTime.parse("16:00")))
			{
				Queue<VOMovingViolations> infraccionesRango17=null;

				String key=fecha.toLocalDate()+"T16";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango17=arbolFechaRango.get(key);
					infraccionesRango17.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango17);
				}
				else
				{
					infraccionesRango17=new Queue<VOMovingViolations>();
					infraccionesRango17.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango17);
				}
			}
			else if((hora.isAfter(LocalTime.parse("17:00"))&&hora.isBefore(LocalTime.parse("17:59")))||hora.equals(LocalTime.parse("17:00")))
			{
				Queue<VOMovingViolations> infraccionesRango18=null;

				String key=fecha.toLocalDate()+"T17";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango18=arbolFechaRango.get(key);
					infraccionesRango18.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango18);
				}
				else
				{
					infraccionesRango18=new Queue<VOMovingViolations>();
					infraccionesRango18.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango18);
				}
			}
			else if((hora.isAfter(LocalTime.parse("18:00"))&&hora.isBefore(LocalTime.parse("18:59")))||hora.equals(LocalTime.parse("18:00")))
			{
				Queue<VOMovingViolations> infraccionesRango19=null;

				String key=fecha.toLocalDate()+"T18";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango19=arbolFechaRango.get(key);
					infraccionesRango19.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango19);
				}
				else
				{
					infraccionesRango19=new Queue<VOMovingViolations>();
					infraccionesRango19.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango19);
				}
			}
			else if((hora.isAfter(LocalTime.parse("19:00"))&&hora.isBefore(LocalTime.parse("19:59")))||hora.equals(LocalTime.parse("19:00")))
			{
				Queue<VOMovingViolations> infraccionesRango20=null;

				String key=fecha.toLocalDate()+"T19";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango20=arbolFechaRango.get(key);
					infraccionesRango20.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango20);
				}
				else
				{
					infraccionesRango20=new Queue<VOMovingViolations>();
					infraccionesRango20.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango20);
				}
			}
			else if((hora.isAfter(LocalTime.parse("20:00"))&&hora.isBefore(LocalTime.parse("20:59")))||hora.equals(LocalTime.parse("20:00")))
			{
				Queue<VOMovingViolations> infraccionesRango21=null;

				String key=fecha.toLocalDate()+"T20";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango21=arbolFechaRango.get(key);
					infraccionesRango21.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango21);
				}
				else
				{
					infraccionesRango21=new Queue<VOMovingViolations>();
					infraccionesRango21.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango21);
				}
			}
			else if((hora.isAfter(LocalTime.parse("21:00"))&&hora.isBefore(LocalTime.parse("21:59")))||hora.equals(LocalTime.parse("21:00")))
			{
				Queue<VOMovingViolations> infraccionesRango22=null;

				String key=fecha.toLocalDate()+"T21";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango22=arbolFechaRango.get(key);
					infraccionesRango22.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango22);
				}
				else
				{
					infraccionesRango22=new Queue<VOMovingViolations>();
					infraccionesRango22.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango22);
				}
			}
			else if((hora.isAfter(LocalTime.parse("22:00"))&&hora.isBefore(LocalTime.parse("22:59")))||hora.equals(LocalTime.parse("22:00")))
			{
				Queue<VOMovingViolations> infraccionesRango23=null;

				String key=fecha.toLocalDate()+"T22";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango23=arbolFechaRango.get(key);
					infraccionesRango23.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango23);
				}
				else
				{
					infraccionesRango23=new Queue<VOMovingViolations>();
					infraccionesRango23.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango23);
				}
			}
			else if((hora.isAfter(LocalTime.parse("23:00"))&&hora.isBefore(LocalTime.parse("23:59")))||hora.equals(LocalTime.parse("23:00")))
			{
				Queue<VOMovingViolations> infraccionesRango23=null;

				String key=fecha.toLocalDate()+"T23";

				if(arbolFechaRango.contains(key))
				{
					infraccionesRango23=arbolFechaRango.get(key);
					infraccionesRango23.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango23);
				}
				else
				{
					infraccionesRango23=new Queue<VOMovingViolations>();
					infraccionesRango23.enqueue(infraccion);
					arbolFechaRango.put(key, infraccionesRango23);
				}
			}
		}

		Iterable<String> g=arbolFechaRango.keys();
		Iterator<String>it=g.iterator();

		while (it.hasNext()) 
		{
			double valorPorFecha=0;
			String key = (String) it.next();
			Queue<VOMovingViolations> colatmp=arbolFechaRango.get(key);
			IteratorQueue<VOMovingViolations> it2=colatmp.iterator();
			while (it2.hasNext()) 
			{
				VOMovingViolations infraccion = (VOMovingViolations) it2.next();
				valorPorFecha+=(infraccion.getFineAMT()+infraccion.getPenalty1()+infraccion.getPenalty2()-infraccion.getTotalPaid());
			}
			if(valorPorFecha>=valorInicial&&valorPorFecha<=valorFinal)
			{
				//	        	2018-01-10T11:46:00.000Z ()
				String fechaInicial=key+":00:00.000Z";
				String fechaFinal=key+":59:59.000Z";

				LocalDateTime horaFechaInicial=ManejoFechaHora.convertirFecha_Hora_LDT(fechaInicial);
				LocalDateTime horaFechaFinal=ManejoFechaHora.convertirFecha_Hora_LDT(fechaFinal);
				InfraccionesFechaHora x=new InfraccionesFechaHora(horaFechaInicial,horaFechaFinal, colatmp);	
				arbolReq.put(valorPorFecha, x);
			}
		}

		Iterable<Double> llaves=arbolReq.keys();
		IteratorQueue<Double>it3=(IteratorQueue<Double>) llaves.iterator();

		while (it3.hasNext()) 
		{
			Double double1 = (Double) it3.next();
			InfraccionesFechaHora element=arbolReq.get(double1);
			colaRta.enqueue(element);
		}

		return colaRta;		
	}

	/**
	 * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	 * @param  int addressID: Localizaci�n de la consulta.
	 * @return Objeto InfraccionesLocalizacion
	 */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		// TODO completar
		String keyAddress=addressID+"";

		Queue<VOMovingViolations> infraccionesAddress=TablaAddress.get(keyAddress);
		VOMovingViolations primeraInfraccion=infraccionesAddress.darPrimerElemento();	
		InfraccionesLocalizacion rta=new InfraccionesLocalizacion(primeraInfraccion.getXcord(), primeraInfraccion.getYcord(), primeraInfraccion.getLocation(), Integer.parseInt(primeraInfraccion.getAddressId()), Integer.parseInt(primeraInfraccion.getStreetSegId()), infraccionesAddress);

		return rta;		
	}

	/**
	 * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	 * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	 * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	 * 		LocalTime horaFinal: Hora final del rango de b�squeda
	 * @return Objeto InfraccionesFranjaHorariaViolationCode
	 */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{

		IQueue<VOMovingViolations> colaInfracciones=new Queue<VOMovingViolations>();
		HashSeparateChaining<String, Queue<VOMovingViolations>> tablaHorasViolation =new HashSeparateChaining<String, Queue<VOMovingViolations>>(100000);
		IQueue<InfraccionesViolationCode> colaViolation=new Queue<InfraccionesViolationCode>();


		String sHoraInicial=horaInicial.toString()+":00";
		String sHoraFinal=horaFinal.toString()+":00";

		Iterable<String> x= arbolHoras.keysInRange(sHoraInicial,sHoraFinal);    
		IteratorQueue<String> it= (IteratorQueue<String>)x.iterator();

		while(it.hasNext())
		{
			String hora=it.next();
			IQueue<VOMovingViolations> listaHoras=arbolHoras.get(hora);
			IQueue<VOMovingViolations> tmp =colaInfracciones.unirColas(listaHoras);
			colaInfracciones=tmp;
		}

		IteratorQueue<VOMovingViolations> it2=(IteratorQueue<VOMovingViolations>) colaInfracciones.iterator();

		while (it2.hasNext()) 
		{
			VOMovingViolations infraccion = (VOMovingViolations) it2.next();

			Queue<VOMovingViolations> infraccionesCodigo=null;

			String key=infraccion.getViolationCode();

			if(tablaHorasViolation.contains(key))
			{
				infraccionesCodigo=tablaHorasViolation.get(key);
				infraccionesCodigo.enqueue(infraccion);
				tablaHorasViolation.put(key, infraccionesCodigo);
			}
			else
			{
				infraccionesCodigo=new Queue<VOMovingViolations>();
				infraccionesCodigo.enqueue(infraccion);
				tablaHorasViolation.put(key, infraccionesCodigo);
			}
		}

		Iterator<String> it3=tablaHorasViolation.keys();

		while (it3.hasNext()) 
		{
			String key = (String) it3.next();
			InfraccionesViolationCode violation=new InfraccionesViolationCode(key, tablaHorasViolation.get(key));
			colaViolation.enqueue(violation);
		} 

		InfraccionesFranjaHorariaViolationCode rta= new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal, colaInfracciones, colaViolation);

		// TODO completar
		return rta;		
	}

	/**
	 * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	 * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	 * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	 * @return Cola de objetos InfraccionesLocalizacion
	 */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		// TODO completar
		IQueue<InfraccionesLocalizacion> colaResp = new Queue<InfraccionesLocalizacion>();
		ArbolRojoNegro<Integer, InfraccionesLocalizacion> arbolRta=new ArbolRojoNegro<Integer, InfraccionesLocalizacion>();

		Iterable<CordVO> x= ArbolUbicacion.keys();    
		IteratorQueue<CordVO> it= (IteratorQueue<CordVO>)x.iterator();
		while (it.hasNext()) 
		{
			CordVO cordVO = (CordVO) it.next();
			InfraccionesLocalizacion localizacion=consultarPorLocalizacionHash(cordVO.getCordX(),cordVO.getCordY());
			arbolRta.put(localizacion.getTotalInfracciones(),localizacion);
		}

		Iterable<Integer> y= arbolRta.keys();
		Queue<Integer> h=(Queue<Integer>) y;
		int tamano=h.darTamano();
		IteratorQueue<Integer> it2= (IteratorQueue<Integer>)y.iterator();
		
		int contadorIterator=0;
		while (it2.hasNext()&&N>0) 
		{
			Integer value = (Integer) it2.next();
			InfraccionesLocalizacion z=arbolRta.get(value);
			if(contadorIterator>=tamano-N)
			{
				colaResp.enqueue(z);
			}
            contadorIterator++;
		}
		
		//Girar la cola de respuestas 
		IteratorQueue<InfraccionesLocalizacion> it3=(IteratorQueue<InfraccionesLocalizacion>) colaResp.iterator();
		InfraccionesLocalizacion[] infracciones=new InfraccionesLocalizacion[colaResp.size()];
		int i=0;
		while(it3.hasNext())
		{
			InfraccionesLocalizacion g=it3.next();
			infracciones[i]=g;
			i++;
		}
	   
		Queue<InfraccionesLocalizacion> colaReversa=new Queue<InfraccionesLocalizacion>();
		for (int j = infracciones.length-1; j > -1; j--) 
		{
			InfraccionesLocalizacion d=infracciones[j]; 
			colaReversa.enqueue(d);
		}
		
		return colaReversa;		
	}

	/**
	 * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	 * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	 */
	public Queue<String> ordenarCodigosPorNumeroInfracciones()
	{
		// TODO completar
		// TODO Definir la Estructura Contenedora
		int numCodes = TablaViolationCode.numKeys();
		MaxHeapCP<ViolationCodeVO> colaP = new MaxHeapCP<ViolationCodeVO>(numCodes);
		Iterator<String> it =TablaViolationCode.keys();
		while(it.hasNext()) {
			String code = it.next();
			int numInf = TablaViolationCode.get(code).size();
			ViolationCodeVO VCVO = new ViolationCodeVO(code);
			for (int i = 0; i < numInf; i++) {
				VCVO.aumentarRegistro();
			}
			System.out.println(VCVO);
			colaP.agregar(VCVO);
		}
		Queue<String> tabla = new Queue<String>();
		for (int i = 0; i < colaP.darNumElementos(); i++) {
			String x = "";
			ViolationCodeVO VCVO = colaP.delMax();
			System.out.println((VCVO.numberOfRegisters * 100000));
			long numInf = Math.abs(((VCVO.numberOfRegisters * 100000)))/ numTotalInfrac;
			int n=(int) ((numInf*10)/100);
			for (int j = 0; j < n; j++) 
			{
				 x+="X";
			} 
			String rta=VCVO.violationCode+"|"+x;
			
			tabla.enqueue(rta);
		}
		return tabla;		
	}


	/**
	 * Retorna los elementos de una cola pasada por parametro en un arreglo.
	 * @return
	 */
	public VOMovingViolations[] getArregloInfracciones(Queue<VOMovingViolations> cola)
	{
		IteratorQueue<VOMovingViolations> it=cola.iterator();
		VOMovingViolations[] infracciones=new VOMovingViolations[cola.size()];
		int i=0;
		while(it.hasNext())
		{
			VOMovingViolations x=it.next();
			infracciones[i]=x;
			i++;
		}
		return infracciones;
	}
	/**
	 * Retorna los elementos de un arreglo pasado por parametro en una cola.
	 * @return
	 */
	public Queue<VOMovingViolations> getColaInfracciones(VOMovingViolations[] arreglo)
	{
		Queue<VOMovingViolations> cola=new Queue<VOMovingViolations>();
		for (int i = 0; i < arreglo.length; i++) 
		{
			cola.enqueue(arreglo[i]);
		}
		return cola;
	}

}
