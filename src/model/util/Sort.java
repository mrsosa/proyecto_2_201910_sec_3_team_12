package model.util;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.IQueue;
import model.data_structures.IteratorQueue;
import model.vo.VOMovingViolations;

/**
 * Clase que organiza los datos por medio de QuickSort.
 * @author nicot
 *
 */
public class Sort 
{
	
/////////////////////////////////////////////////////////////////MERGESORT
	
	/**
	 * Arreglo auxiliar usado en el mergesort
	 */
	private static Comparable[] auxiliar;

	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ,String sTipo) 
	{
		// TODO implementar el algoritmo MergeSort
		Msort(datos, 0, datos.length, sTipo); 
	}
	/**
	 * Une dos arreglos organizando los elementos en un tercer arreglo pasado por parametro
	 * @param arreglo a organizar
	 * @param low index inicial.
	 * @param mid index que marca la mitad del arreglo a oredenar
	 * @param high index que marca el final del arreglo a ordenar.
	 */
	public static void merge(Comparable[] arreglo,int low,int mid,int high,String sTipo)
	{
		// Unir los arreglos de[low..mid] con [mid+1..high].   
		int i = low; 
		int j = mid+1;  

		// Copiar desde[low..high] a auxiliar[low..high].
		for (int y = low; y <= high; y++)        
			auxiliar[y] = arreglo[y];  

		// Unir los dos arreglos en el arreglo original, IMPORTANTE::: recordar la precedencia de i++(cuadra i y despues le suma 1) 

		for (int k = low; k <= high; k++)
		{
			if      (i > mid)              arreglo[k] = auxiliar[j++];      
			else if (j > high )            arreglo[k] = auxiliar[i++];     
			else if (less(auxiliar[j], auxiliar[i],sTipo)) arreglo[k] = auxiliar[j++];     
			else                           arreglo[k] = auxiliar[i++];
		}       
	}
	/**
	 * Da una primera instruccion para empezar a organizar el arreglo
	 * @param arr arreglo a organizar
	 * @param low index menor 
	 * @param highindex mayor
	 */
	public static void Msort(Comparable[] arr,int low, int high,String sTipo)
	{
		auxiliar = new Comparable[arr.length];    // Crea el espacio del arreglo auxiliar una sola vez,     
		sortR(arr, 0, arr.length - 1,sTipo); //Lo organiza recursivamente
	}
	/**
	 * Organiza por medio de recursion el arreglo, partiendolo en dos mitades y uniendolas por el metodo merge.
	 * @param arr arreglo de organizar
	 * @param low index menor
	 * @param high index mayor
	 */
	private static void sortR(Comparable[]arr,int low, int high,String sTipo)
	{
		if(high<=low) return;
		int mid = low + (high - low)/2; //Calcula la mitad del arreglo      
		sortR(arr, low, mid,sTipo);       // Organiza la mitad izquierda.      
		sortR(arr, mid+1, high,sTipo);     //  Organiza la mitad derecha.    
		merge(arr, low, mid, high,sTipo);  //Une las dos mitades
	}
	
	
	/////////////////////////////////////////////////////////////////QUICKSORT
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos, String sTipo) 
	{
		// TODO implementar el algoritmo QuickSort
		shuffle(datos);
		Qsort(datos, 0, datos.length - 1, sTipo);
	}
	
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @param s Tipo de Comparacion a realizar
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w, String s)
	{
		// TODO implementar
		boolean a = false;
		if(s.equals("addressId"))
		{
			a=v.compareTo(w)<0;
		}
		else if(s.equals("coord"))
		{
			a=((VOMovingViolations) v).compareToCoord((VOMovingViolations) w)<0;
		}
		else
		{
			a=v.compareTo(w)<0;
		}
		return a;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable tmp = datos[i]; 
		datos[i] = datos[j]; 
		datos[j] = tmp;
	}

	////////////////METODOS DEL QUICKSORT///////////////
	/**
	 * Organiza recursivamente un arreglo dividiendolo por izq y por derecha
	 * @param a
	 * @param low
	 * @param high
	 */
	private static void Qsort(Comparable[] a, int low, int high, String s)
	{
		if(high <= low) return;
		int j = partition(a, low, high, s);
		Qsort(a, low, j-1, s);
		Qsort(a, j+1, high, s);		
	}
	/**
	 * Parte un arreglo en varios sub arreglos y realiza cambios para ordenarlos
	 * @param a
	 * @param low
	 * @param high
	 * @return
	 */
	private static int partition(Comparable[] a, int low, int high, String s)
	{
		int i = low, j = high + 1;
		Comparable v = a[low];
		while(true){
			while(less(a[++i], v, s)) if(i == high) break;
			while(less(v, a[--j], s)) if(j == low) break;
			if(i >= j) break;
			exchange(a,i,j);
		}
		exchange(a,low,j);
		return j;
	}
	/**
	 * Baraja un arreglo aleatoriamente
	 * @param datos
	 */
	private static void shuffle(Comparable[] datos)
	{
		int index=datos.length-1;
		Comparable<VOMovingViolations> temp;
		for (int j=index; j >1; j--) 
		{
			int aleatorio=(int) (Math.random()*j);  //Da un numero aleatorio entre 0 y n.
			temp=datos[j];   //Hace una variable temporal para guardar el elemento j.
			datos[j]=datos[aleatorio];  //Asigna a la posicion J un elemento aleatorio.
			datos[aleatorio]=temp;   //Guarda en la posicion j el elemento aleatorio.
		}
	}

	/////////////////////////////////////OTROS METODOS ADICIONALES//////////////////////////////////////
	
	/**
	 * Metodo para consegir el arreglo de una cola generica.
	 * @param <T>
	 * @param <T>
	 * @param s  cola a conseguir el arreglo
	 * @return arreglo en base la cola que llega por parametro.
	 */
	public static <T> Comparable[] getArreglo(IQueue<T> s)
	{
		IteratorQueue<T> it= (IteratorQueue<T>) s.iterator();
		Comparable[] infracciones =  new Comparable[s.size()];
		int i=0;
		while(it.hasNext())
		{
			T x=it.next();
			infracciones[i]= (Comparable<T>) x;
			i++;
		}

		return infracciones;
	}


}