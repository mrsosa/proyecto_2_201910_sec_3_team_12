package model.vo;

//vo ES VALUE OBJECT
public class CordVO implements Comparable<CordVO>
{
	/**
	 * Coordernada en x
	 */
    public Double cordX;
	/**
	 * Coordernada en y
	 */
    public Double cordY;
  
	/**
	 * Constructor de la clase
	 */
	public CordVO(Double pCordX,Double pCordY ) 
	{
		cordX=pCordX;
		cordY=pCordY;
	}

	/**
	 * Retorna la coordenada en X	
	 * @return
	 */
    public Double getCordX() 
    {
		return cordX;
	}
	/**
	 * Retorna la coordenada en Y	
	 * @return
	 */
	public Double getCordY() 
	{
		return cordY;
	}
	
	/**
	 * Compara dos LovationVo por su numero de registros si son iguales los compara por orden alfabetico de sus locaciones.
	 */
	@Override
	public int compareTo(CordVO o) 
	{
		// TODO Auto-generated method stub
		//Rta es positivo si el numero de registros es mayor al que me pasan por parametro.
		//Rta es negativo si el numeor de registros es menor al que me pasan por parametro.
		int rta=cordX.compareTo(o.cordX);
		if(rta==0)
		{
		   rta=cordY.compareTo(o.cordY);	
		}
		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return cordX+","+cordY;
	}
}
