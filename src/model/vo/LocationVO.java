package model.vo;

public class LocationVO implements Comparable<LocationVO>
{

	/**
	 * Address Id de la Infraccion.
	 */
	public int addressId;
	/**
	 * String que describe la ubicacion del Lugar
	 */
	public String location;
	/**
	 * Numero de registros que sucedieron en el lugar
	 */
	public int numberOfRegisters;

	/**
	 * Constructor de la clase
	 */
	public LocationVO(int pAddressId,String pLocation) 
	{
		addressId=pAddressId;
		location=pLocation;
		numberOfRegisters=1;
	}
	/**
	 * Aumenta en uno el numero de registros del LocationVO
	 */
	public void aumentarRegistro()
	{
		numberOfRegisters++;
	}
	/**
	 * Retorna el adressId
	 */
	public int getAddressId()
	{
		return addressId;
	}
	/**
	 * Retorna la locacion del objeto.
	 */
	public String getLocation()
	{
		return location;
	}
	/**
	 * Retorna el numero de registros
	 */
	public int getNumberOfRegisters()
	{
		return numberOfRegisters;
	}
	/**
	 * Compara dos LovationVo por su numero de registros si son iguales los compara por orden alfabetico de sus locaciones.
	 */
	@Override
	public int compareTo(LocationVO o) 
	{
		// TODO Auto-generated method stub
		//Rta es positivo si el numero de registros es mayor al que me pasan por parametro.
		//Rta es negativo si el numeor de registros es menor al que me pasan por parametro.
		
		int rta=numberOfRegisters-o.numberOfRegisters;
		if(rta==0)
		{
		   return location.compareTo(o.location);	
		}
		
		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "La Ubicacion: "+location + " tienen como adressId: "+addressId+" y tiene "+numberOfRegisters +" infracciones registradas";
	}
}
