package model.vo;

public class ViolationCodeVO implements Comparable<ViolationCodeVO>
{
	/**
	 * Address Id de la Infraccion.
	 */
	public String violationCode;
	/**
	 * Numero de registros que sucedieron en el lugar
	 */
	public int numberOfRegisters;

	/**
	 * Constructor de la clase
	 */
	public ViolationCodeVO(String pViolation) 
	{
		violationCode=pViolation;
		numberOfRegisters=1;
	}
	/**
	 * Aumenta en uno el numero de registros del LocationVO
	 */
	public void aumentarRegistro()
	{
		numberOfRegisters++;
	}
	/**
	 * Retorna el numero de registros
	 */
	public int getNumberOfRegisters()
	{
		return numberOfRegisters;
	}
	/**
	 * retorna el codigo de violacion.
	 */
	public String getViolationCode()
	{
		return violationCode;
	}
	/**
	 * Compara dos LovationVo por su numero de registros si son iguales los compara por orden alfabetico de sus locaciones.
	 */
	@Override
	public int compareTo(ViolationCodeVO o) 
	{
		// TODO Auto-generated method stub
		//Rta es positivo si el numero de registros es mayor al que me pasan por parametro.
		//Rta es negativo si el numeor de registros es menor al que me pasan por parametro.
		
		int rta=numberOfRegisters-o.numberOfRegisters;
		if(rta==0)
		{
		   return violationCode.compareTo(o.violationCode);	
		}
		
		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "El codigo: "+violationCode+ " tiene "+numberOfRegisters +" infracciones registradas";
	}
}
