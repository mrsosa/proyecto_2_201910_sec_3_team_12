package model.data_structures;


import java.util.Iterator;

import org.junit.Before;
import junit.framework.TestCase;
import model.data_structures.HashSeparateChaining;

/**
 * Pruebas sobre la calse TestSeparateChaining
 * @author na.tobo
 */
public class TestSeparateChaining extends TestCase
{
	/**
	 * Atributo que indica el numero de tuplas de la tabla;
	 */
	private int numTuplas;
	/**
	 * Atributo que indica el tama�o inicial de la tabla;
	 */
	private int tamInicial;
	/**
	 * Atributo que indica el tama�o Final de la tabla;
	 */
	private int tamFinal;
	/**
	 * Long que reconoce el tiempo Incial
	 */ 
	private long tInicial;
	/**
	 * Long que reconoce el tiempo Final
	 */ 
	private long tFinal;

	/**
	 * Tabla hashSeparateChaining que va realizar pruebas sobre tuplas integers-Strings.
	 */
	private HashSeparateChaining<Integer,String> tabla;

	/**
	 * Monta el escenario para probar la tabla de hash
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		tamInicial=10000;
		tabla=new HashSeparateChaining<>(tamInicial);
	}
	/**
	 * Inserta elementos dentro de la tabla
	 */
	public void testPut()
	{
		for (int i = 0; i < 10000; i++) 
		{
			tabla.put(i,"prueba"+i);
		}
		assertEquals("Deberia haber insertado todas las llaves: ",10000,tabla.numKeys());

	}
	/**
	 * Prueba la funcion de Rehash de la tabla 
	 */
	public void testRehash()
	{
		for (int i = tabla.numKeys(); i < 400000; i++) 
		{
			tabla.put(i,"prueba"+i);
		}
		assertEquals("Deberia tener un tama�o de 80000",80000,tabla.size());
		assertEquals("Deberia haber realizado la operacion rehash 3 veces ",3,tabla.numRehash());
		numTuplas=tabla.numKeys();
		tamFinal=tabla.size();
	}
	/**
	 * Prueba la funcion de Rehash de la tabla 
	 */
	public void testGet()
	{
		try 
		{
			testPut();
			testRehash();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String x=tabla.get(0);

		assertEquals("Deberia haber retornado el string correcto","prueba0",x);

		int aleatorio=(int) (Math.random()*tabla.size());

		tInicial=System.currentTimeMillis();

		String y=tabla.get(aleatorio);

		tFinal=System.currentTimeMillis();

		assertNotNull(y);
	}
	/**
	 * Prueba la funcion keys()
	 */
	public void testKeys()
	{
		try 
		{
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Iterator<Integer> it=tabla.keys();
		int cont=0;
		while(it.hasNext())
		{
			it.next();
			cont++;
		}
		assertEquals("Deberia haber realizado el ciclo sobre todas las llaves",cont,tabla.numKeys());

		ImprimirRes();
	}



	/**
	 * Prueba la funcion keys()
	 */
	public void ImprimirRes()
	{
		try 
		{
			testPut();
			testGet();
			testRehash();
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double fc=tabla.numKeys()/tabla.size();
		long tPromedio=(tFinal-tamInicial)/tabla.size();

		System.out.println("Tama�o Inicial: "+tamInicial);
		System.out.println("Tama�o Final: "+tamFinal);
		System.out.println("Numero de duplas en la tabla: "+numTuplas);
		System.out.println("Factor de carga final: "+ fc);
		System.out.println("Numero de rehashes: "+ tabla.numRehash());
		System.out.println("Tiempo promedio get: "+ (tPromedio/1000));		
	}
}
